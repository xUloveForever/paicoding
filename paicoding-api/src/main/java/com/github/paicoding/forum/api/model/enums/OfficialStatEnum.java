package com.github.paicoding.forum.api.model.enums;

import lombok.Getter;

/**
 * 官方状态枚举
 *
 * @author louzai
 * @since 2022/7/19
 */
@Getter
public enum OfficialStatEnum {

    NOT_OFFICAL(0, "非官方"),
    OFFICAL(1, "官方");

    OfficialStatEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private final Integer code;
    private final String desc;

    public static OfficialStatEnum formCode(Integer code) {
        for (OfficialStatEnum value : OfficialStatEnum.values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        return OfficialStatEnum.NOT_OFFICAL;
    }
}
